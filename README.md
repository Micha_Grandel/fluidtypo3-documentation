IMPORTANT NOTE
==============

You may read the documentation at https://fluidtypo3.org/documentation/templating-manual/introduction.html.

This repo is cloned from https://github.com/FluidTYPO3/documentation

**Please use the original repository for any issues or contribution!**
The purpose of this clone is only to own a copy that I can control - as a backup of the documentation for my bachelor thesis!

The clone has been created at 11/21/2018.

Fluid Powered TYPO3: Documentation
==================================

> Welcome to the documentation collection for the extensions in the Fluid Powered TYPO3 family.

The documentation collection is divided into the following primary chapters:

* [Introduction](Introduction.md)
  Is a short description of what Fluid Powered TYPO3 is all about - in bullet point form.
* [Online Services](OnlineServices.md)
  Contains links to references for ViewHelpers, links to our support chat, links to download packages and generated extensions.
* [Beginner's Guide](BeginnersGuide.md)
  Is a book-form guide which can be read from end to end. It teaches you the basic concepts shared by the extensions.
* [Best Practices](BestPractice/)
  Is a set of best practices for all areas involved in creating web sites based on Fluid TYPO3 extensions. It contains:
  * [Best practices for Configuration](BestPractice/Configuration.md)
  * [Best practices for Code Building](BestPractice/CodeBuilding.md)
  * [Best practices for Migrating Resources and Setups](BestPractice/Migration.md)
  * [Best practices for Content Template Creation](BestPractice/Content.md)
  * [Best practices for Page Template Creation](BestPractice/Pages.md)
  * [Best practices for Plugin Integration](BestPractice/Plugins.md)
* [Guides](Guides/)
  Is a set of guides for using the Flux API. It contains:
  * [Guide for using the FormComponent](Guides/FormComponent.md)
  * [Guide for using language files and translations](Guides/WorkingWithLocallang.md)
* [Extensions](Extensions.md)
  Is a more detailed set of documentation about each extension's features, requirements, practices etc.
* [Concepts](Concepts/)
  Contains in-depth information about unique concepts Fluid Powered TYPO3 introduces.
* [Cookbook](Cookbook/)
  Is a collection of code examples.
* [Contributing](Contributing/)
  Is for those of you who wish to contribute code. It describes our workflow and quality expectations.

## Contributing to the documentation collection

> Wish to contribute? Have a correction? Great! :)

Please refer to the [original repo](https://github.com/FluidTYPO3/documentation). They will welcome your contribution!